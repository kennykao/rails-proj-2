# Title: OOTD (Outfit Of The Day) App
Team Members: Kenny Kao, Tim Lim, Helen Wang, Jeremy Wan, Chris Le

## Demo Link: https://youtu.be/uVLHFzfka9s

## Idea: An application where users can create OutfitOfTheDay accounts, post their own outfits, and rate outfits/ collections to their liking.

## Models and Description:

### User
* has name, email, and outfits
* users have their own outfit collections, and can rate other outfits
### Outfit
* has many Items that fit an outfit
* can be “rated” by user
### Item
* belongs to an outfit
* has name and image associated with it

## Features:
* Users can login and make an account
* Users can create/ name their own outfits
* Users can upload pictures for their items within outfits
* Users can put url’s of pictures for outfits
* Users can rate outfits

## Division of Labor:
* Kenny: Implemented ratyrate gem and front end
* Tim: Helped implement CarrierWave gem and features of “outfits”
* Helen: Wrote the seed file that was probably overwritten by Chris Le later
* Jeremy: Helped with video and overall planning process
* Chris: front end, back end, mid end,  motivational speaker, git master, kind of everything
