class OutfitsController < ApplicationController
  def index
    @outfits = Outfit.all
  end

  def show
    @outfit = Outfit.find(params[:id])
  end

  def new
    @outfit = Outfit.new()
  end

  def create
     puts current_user
     @outfit = Outfit.new(name:params[:outfit][:name], user_id:current_user)
     if @outfit.save
       flash[:notice] = "Successfully created outfit."
       redirect_to @outfit
     else
       render :action => 'new'
     end
   end

   def edit
     @outfit = Outfit.find(params[:id])
   end

   def update
     @outfit = Outfit.find(params[:id])
     if @outfit.update_attributes(params[:outfit])
       flash[:notice] = "Successfully updated outfit."
       redirect_to outfit_url
     else
       render :action => 'edit'
     end
   end

   def destroy
     @outfit = Outfit.find(params[:id])
     @outfit.destroy
     flash[:notice] = "Successfully destroyed outfit."
     redirect_to outfits_url
   end

end
