class ItemsController < ApplicationController

  def new
    @item = Item.new(:outfit_id => params[:outfit_id])
  end

  def create
    @item = Item.new(outfit_id:params[:item][:outfit_id], name:params[:item][:name], image:params[:item][:image])
    if @item.save
      flash[:notice] = "Successfully created item."
      redirect_to outfit_path(params[:item][:outfit_id])
    else
      render :action => 'new'
    end
  end

  def edit
    @item = Item.find(params[:id])
  end

  def update
    @item = Item.find(params[:id])
    if @item.update_attributes(params[:item])
      flash[:notice] = "Successfully updated item."
      redirect_to @item.outfit
    else
      render :action => 'edit'
    end
  end

  def destroy
    @item = Item.find(params[:id])
    @item.destroy
    flash[:notice] = "Successfully destroyed item."
    redirect_to @item.outfit
  end

end
